#ifndef userSetting_H
#define userSetting_H
#include <QTextEdit>
#include <windows.h>
#include <vector>
#include <lm.h>
#include <QCheckBox>

class userSetting
{
public:

    enum typeOfCommand {
        DISABLEUSER,
        CHANGEPASSWORD,
        REMOVEFROMGROUP,
        CHANGEACCOUNTNAME,
        SETPASSWORDEXPERATION

    };

    userSetting();
    void setCommand(userSetting::typeOfCommand command);
    void getInputUsersList();
    void determineBadAdmins();
    void setWidget(QTextEdit *users,QTextEdit *admins,QCheckBox *disableGuest,QCheckBox *disableAdmin);
    void determineBadUsers();
    std::string getCommandString();
    QString getWidgetValueUsers();
    QString getWidgetValueAdmins();
    void getLocalUsers();
    void getLocalAdmins();
    void getInputAdminsList();

private:

    typeOfCommand _command;
    std::vector <std::string> _localUsers;
    std::vector <std::string> _localAdmins;
    std::vector <char> _usersTEMP;
    std::vector<std::string> _inputUsers;
    std::vector<std::string> _inputAdmins;

    QTextEdit * _textEditUsers;
    QTextEdit * _textEditAdmins;
    QCheckBox * _disableGuest;
     QCheckBox * _disableAdmin;
    LPCWSTR _user;
};

#endif // userSetting_H
