#include "auditSetting.h"
#include <string>


auditSetting::auditSetting()
{
    _success = "disable";
    _failure = "disable";
}

void auditSetting::setCommand(auditSetting::typeOfCommand command)
{
    _command = command;
}
void auditSetting::setWidget(QCheckBox *success ,QCheckBox *failure){

    successBox = success;
    failureBox = failure;

}
void auditSetting::getWidgetValue(){

    if(successBox->isChecked() != 0){

        _success= "enable";

    }else{


        _success= "disable";

    }

    if (failureBox->isChecked() != 0){


        _failure = "enable";

    }else{


        _failure = "disable";


    }
}

std::string auditSetting::getCommandString()
{

    this->getWidgetValue();
    std::string command;
    switch(_command){



    case SYSTEM:

       command = "auditpol /set /category:\"System\" /success:"+_success+
               " /failure:"+_failure;
        break;


    case LOGONOFF:

         command = "auditpol /set /category:\"Logon/Logoff\" /success:"+_success+
                 " /failure:"+_failure;
          break;


    case OBJECTACC:

         command = "auditpol /set /category:\"Object Access\" /success:"+_success+
                 " /failure:"+_failure;
            break;


    case PRIVELEDGEUSE:

        command = "auditpol /set /category:\"Privilege Use\" /success:"+_success+
                " /failure:"+_failure;
        break;

    case TRACKING:

        command = "auditpol /set /category:\"Detailed Tracking\" /success:"+_success+
                " /failure:"+_failure;
        break;

    case POLCIYCHANGE:

        command = "auditpol /set /category:\"Policy Change\" /success:"+_success+
                " /failure:"+_failure;
        break;

    case ACCOUNTMAN:

        command = "auditpol /set /category:\"Account Managment\" /success:"+_success+
                " /failure:"+_failure;
        break;

    case DSACCESS:

        command = "auditpol /set /category:\"DS Access\" /success:"+_success+
                " /failure:"+_failure;
        break;

    case ACCOUTLOGON:

        command = "auditpol /set /category:\"Account Logon\" /success:"+_success+
                " /failure:"+_failure;
        break;

    }


    return command;

}
int auditSetting::getBool()
{
    //return _success;
}

