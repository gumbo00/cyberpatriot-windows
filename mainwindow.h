#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include "cpsecurity.h"
#include <QtWidgets>
#include <passwordSetting.h>
#include <auditsetting.h>
#include "usersetting.h"

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_unlockButtonPasswords_clicked();


private:
    Ui::MainWindow *ui;
    CPSecurity cyberPatriot;
    void enumerateArrays();

    passwordSetting maxpwage,minpwage,minpwlen,uniquepw,reversecrypt,enforcecomplex,
    accountLockoutThresh,resetLockoutTimer,accountLockoutDur;

    passwordSetting * passwordExecutes[7];

    auditSetting system, logonoff, objectacc, priveledgeuse, tracking,
    policychange, accountman, dsaccess, accountlogon;

    auditSetting * auditExecutes[9];

    userSetting disableUsers;

};

#endif // MAINWINDOW_H
