#include "passwordSetting.h"
#include <string>
#include <stdlib.h>

using namespace std;
passwordSetting::passwordSetting()
{
    //_command = command;


    _numeric = 0;
}

void passwordSetting::setWidget(QSpinBox *spinbox)
{
    _spinbox = spinbox;
}

void passwordSetting::setCommand(passwordSetting::typeOfCommand command)
{
    _command = command;
}

std::string passwordSetting::getCommandString()
{
    std::string command;
    _numeric = this->getWidgetValue();
    switch(_command){

    case MINPWLEN:

       command = "net accounts /minpwlen:"+to_string(_numeric);
        break;


    case MAXPWAGE:

         command = "net accounts /maxpwage:"+to_string(_numeric);
          break;


    case MINPWAGE:

         command = "net accounts /minpwage:"+to_string(_numeric);
            break;


    case UNIQUEPW:

        command = "net accounts /uniquepw:"+to_string(_numeric);
        break;

    case ACCOUNTLOCKOUTTHRESHOLD:
            command = "net accounts /lockoutthreshold:"+to_string(_numeric);
    break;

    case ACCOUNTLOCKOUTDURATION:
            command = "net accounts /lockoutduration:"+to_string(_numeric);
    break;

    case RESETLOCKOUTTIMER:
            command = "net accounts /lockoutwindow:"+to_string(_numeric);
    break;

    }

    return command;

}
int passwordSetting::getNumeric()
{
    return _numeric;
}

int passwordSetting::getWidgetValue(){

    return _spinbox->value();
}

