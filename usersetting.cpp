#ifndef UNICODE
#define UNICODE
#endif
#include "usersetting.h"
#include <string>
#include <iostream>
#include <stdlib.h>
#include <QString>
#include <QDebug>
#include <stdio.h>
#include <assert.h>
#include <windows.h>
#include <lm.h>
#include <activeds.h>
#include <Adshlp.h>


using namespace std;
userSetting::userSetting()
{
    this->getLocalUsers();
    this->getLocalAdmins();

}
void userSetting::setCommand(userSetting::typeOfCommand command){


}
void userSetting::setWidget(QTextEdit *users, QTextEdit *admins, QCheckBox *disableGuest, QCheckBox *disableAdmin){


    _textEditUsers = users;
    _textEditAdmins = admins;
    _disableGuest = disableGuest;
    _disableAdmin = disableAdmin;
}

void userSetting::getInputUsersList(){
   std::string users = getWidgetValueUsers().toStdString();
   char userss[users.length()];
   strcpy(userss, users.c_str());

   int i = 0;
    for(char o : userss){
        i++;
       if(o == '\n'){
          _inputUsers.push_back(std::string(_usersTEMP.begin(), _usersTEMP.end()));
          _usersTEMP.clear();

       }else if(i == users.length()){
       _usersTEMP.push_back(o);
       _inputUsers.push_back(std::string(_usersTEMP.begin(), _usersTEMP.end()));
       _usersTEMP.clear();

       }else{

           _usersTEMP.push_back(o);

       }
    }
}

void userSetting::getInputAdminsList(){
   std::string users = getWidgetValueAdmins().toStdString();
   char userss[users.length()];
   strcpy(userss, users.c_str());

   int i = 0;
    for(char o : userss){
        i++;
       if(o == '\n'){
          _inputAdmins.push_back(std::string(_usersTEMP.begin(), _usersTEMP.end()));
          _usersTEMP.clear();

       }else if(i == users.length()){
       _usersTEMP.push_back(o);
        _inputAdmins.push_back(std::string(_usersTEMP.begin(), _usersTEMP.end()));
       _usersTEMP.clear();

       }else{

           _usersTEMP.push_back(o);

       }
    }
}


std::string userSetting::getCommandString(){


}
void userSetting::getLocalAdmins(){
    PLOCALGROUP_MEMBERS_INFO_1 pBuf = NULL;
    PLOCALGROUP_MEMBERS_INFO_1 pTmpBuf;
    DWORD dwLevel = 1;
    DWORD dwPrefMaxLen = MAX_PREFERRED_LENGTH;
    DWORD dwEntriesRead = 0;
    DWORD dwTotalEntries = 0;
    DWORD dwResumeHandle = 0;
    DWORD i;
    DWORD dwTotalCount = 0;
    NET_API_STATUS nStatus;
    LPTSTR pszServerName = NULL;
    LPTSTR group = L"Administrators";

    char buffer[500];
      do
      {
         nStatus = NetLocalGroupGetMembers((LPCWSTR) pszServerName,
                               group,
                               dwLevel,
                               (LPBYTE*)&pBuf,
                               dwPrefMaxLen,
                               &dwEntriesRead,
                               &dwTotalEntries,
                               &dwResumeHandle);

         if ((nStatus == NERR_Success) || (nStatus == ERROR_MORE_DATA))
         {
            if ((pTmpBuf = pBuf) != NULL)
            {
               for (i = 0; (i < dwEntriesRead); i++)
               {
                  assert(pTmpBuf != NULL);

                  if (pTmpBuf == NULL)
                  {
                     fprintf(stderr, "An access violation has occurred\n");
                     break;
                  }
                        wcstombs(buffer, pTmpBuf->lgrmi1_name, 500);
                        std::string d = buffer;
                        _localAdmins.push_back(d);
                  pTmpBuf++;
                  dwTotalCount++;
               }
            }
         }

         else
            fprintf(stderr, "A system error has occurred: %d\n", nStatus);

         if (pBuf != NULL)
         {
            NetApiBufferFree(pBuf);
            pBuf = NULL;
         }
      }

      while (nStatus == ERROR_MORE_DATA);

      if (pBuf != NULL)
         NetApiBufferFree(pBuf);


}
void userSetting::determineBadUsers(){
    this->getInputUsersList();
    char buffer[500];

    for(int i = 0; i < _localUsers.size(); i++){
        for(int v = 0; v < _inputUsers.size(); v++){
        if(_localUsers[i] == _inputUsers[v]){
                _localUsers.erase(_localUsers.begin()+i);

            }
        }
    }

 for(int i = 0; i < _localUsers.size(); i++){

         DWORD dwLevel = 1008;
         USER_INFO_1008 ui;
         NET_API_STATUS nStatus;
         std::wstring stemp(_localUsers[i].begin(),_localUsers[i].end());
         LPCWSTR s = stemp.c_str();

           ui.usri1008_flags = UF_SCRIPT | UF_ACCOUNTDISABLE;

                 nStatus = NetUserSetInfo(NULL,
                                       s,
                                       dwLevel,
                                       (LPBYTE)&ui,
                                       NULL);

          // if (nStatus == NERR_Success)
                 //fwprintf(stderr, L"User account %s has been disabled\n", wString);
         // else
                //fprintf(stderr, "A system error has occurred: %d\n", nStatus);


    }
}

void userSetting::determineBadAdmins(){

    this->getInputAdminsList();
    char buffer[500];
    for(int i = 0; i < _localAdmins.size(); i++){
        for(int v = 0; v < _inputAdmins.size(); v++){
        if(_localAdmins[i] == _inputAdmins[v]){
                _localAdmins.erase(_localAdmins.begin()+i);

        }
    }

        /*for(int g = 0; g < _localAdmins.size(); g++){
        cout << _localAdmins[g];
        }*/
  }

 for(int g = 0; g < _localAdmins.size(); g++){

         DWORD dwLevel = 3;
         NET_API_STATUS nStatus;
         LOCALGROUP_MEMBERS_INFO_3 userMemeberInfo;
         LOCALGROUP_MEMBERS_INFO_3 users[1];
         std::wstring stemp(_localAdmins[g].begin(),_localAdmins[g].end());
         string s(stemp.begin(),stemp.end());
         LPCTSTR userName = stemp.c_str();
         LPTSTR group = L"Administrators";
         DWORD entries = _localAdmins.size();

         cout << _localAdmins[g];

        userMemeberInfo.lgrmi3_domainandname = (LPWSTR)userName;
         users[0] = userMemeberInfo;

                 nStatus = NetLocalGroupDelMembers((LPCWSTR)NULL,
                                       (LPCWSTR)group,
                                       dwLevel,
                                       (LPBYTE)&users,
                                       entries);

           //if (nStatus == NERR_Success)
                 //fwprintf(stderr, "User account %s has been disabled\n", userName);
          //else
                //fprintf(stderr, "A system error has occurred: %d\n", nStatus);

    }
}

QString userSetting::getWidgetValueUsers(){

    QString users =_textEditUsers->toPlainText();

    return users;

}

QString userSetting::getWidgetValueAdmins(){

    QString users =_textEditAdmins->toPlainText();

    return users;

}
void userSetting::getLocalUsers(){
       LPUSER_INFO_0 pBuf = NULL;
       LPUSER_INFO_0 pTmpBuf;
       DWORD dwLevel = 0;
       DWORD dwPrefMaxLen = MAX_PREFERRED_LENGTH;
       DWORD dwEntriesRead = 0;
       DWORD dwTotalEntries = 0;
       DWORD dwResumeHandle = 0;
       DWORD i;
       DWORD dwTotalCount = 0;
       NET_API_STATUS nStatus;
       LPTSTR pszServerName = NULL;

       char buffer[500];


         do // begin do
         {
            nStatus = NetUserEnum((LPCWSTR) pszServerName,
                                  dwLevel,
                                  FILTER_NORMAL_ACCOUNT, // global users
                                  (LPBYTE*)&pBuf,
                                  dwPrefMaxLen,
                                  &dwEntriesRead,
                                  &dwTotalEntries,
                                  &dwResumeHandle);
            //
            // If the call succeeds,
            //
            if ((nStatus == NERR_Success) || (nStatus == ERROR_MORE_DATA))
            {
               if ((pTmpBuf = pBuf) != NULL)
               {

                  for (i = 0; (i < dwEntriesRead); i++)
                  {
                     assert(pTmpBuf != NULL);

                     if (pTmpBuf == NULL)
                     {
                        fprintf(stderr, "An access violation has occurred\n");
                        break;
                     }
                     wcstombs(buffer,pTmpBuf->usri0_name,sizeof(buffer)+1);


                    // _user = pTmpBuf->usri0_name;

                     std::string b = buffer;

                     //QString qstr = QString::fromStdString(b);
                     _localUsers.push_back(b);
                     pTmpBuf++;
                     dwTotalCount++;
                  }
               }
            }
            //
            // Otherwise, print the system error.
            //
            else
               fprintf(stderr, "A system error has occurred: %d\n", nStatus);
            //
            // Free the allocated buffer.
            //
            if (pBuf != NULL)
            {
               NetApiBufferFree(pBuf);
               pBuf = NULL;
            }
         }
         // Continue to call NetUserEnum while
         //  there are more entries.
         //
         while (nStatus == ERROR_MORE_DATA); // end do
         //
         // Check again for allocated memory.
         //
         if (pBuf != NULL)
            NetApiBufferFree(pBuf);
         //
         // Print the final count of users enumerated.
         //
         //fprintf(stderr, "\nTotal of %d entries enumerated\n", dwTotalCount);

         for(int i = 0; i < _localUsers.size();i++){
         DWORD dwLevel2 = 1008;
         USER_INFO_1008 ui;
         NET_API_STATUS nStatus2;
         //wchar_t* wString= new wchar_t[4096];
         std::wstring stemp(_localUsers[i].begin(),_localUsers[i].end());
         LPCWSTR s = stemp.c_str();
         //MultiByteToWideChar(CP_ACP, 0, _localUsers[i].c_str(), -1, wString, 4096);

           ui.usri1008_flags = UF_SCRIPT;

                 nStatus2 = NetUserSetInfo(NULL,
                                       s,
                                       dwLevel2,
                                       (LPBYTE)&ui,
                                       NULL);}
}
