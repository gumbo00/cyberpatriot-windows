#ifndef auditSetting_H
#define auditSetting_H
#include <QCheckBox>

class auditSetting
{
public:

    enum typeOfCommand {
        SYSTEM = 1,
        LOGONOFF = 2,
        OBJECTACC = 3,
        PRIVELEDGEUSE = 4,
        TRACKING =5,
        POLCIYCHANGE = 6,
        ACCOUNTMAN = 7,
        DSACCESS = 8,
        ACCOUTLOGON = 9

    };

    auditSetting();
    void setCommand(auditSetting::typeOfCommand command);
    void setSuccess(bool success);
    void setFailure(bool success);
    void setBool(bool value);
    void setWidget(QCheckBox *success,QCheckBox *failure);
    void getWidgetValue();
    int getBool();
    std::string getCommandString();

private:

    typeOfCommand _command;
    std::string _success;
    std::string _failure;
    QCheckBox *successBox, *failureBox;
};

#endif // auditSetting_H
