#-------------------------------------------------
#
# Project created by QtCreator 2014-10-26T17:17:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Take2
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11
LIBS += "C:/windows/System32/netapi32.dll"
LIBS += "C:/windows/System32/activeds.dll"
INCLUDEPATH += c:/windows/System32/activeds.dll
SOURCES += main.cpp\
        mainwindow.cpp \
    cpsecurity.cpp \
    passwordSetting.cpp \
    auditsetting.cpp \
    usersetting.cpp

HEADERS  += mainwindow.h \
    cpsecurity.h \
    passwordSetting.h \
    auditsetting.h \
    usersetting.h

FORMS    += mainwindow.ui
