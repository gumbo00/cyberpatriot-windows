#ifndef passwordSetting_H
#define passwordSetting_H
#include <QSpinBox>

class passwordSetting
{
public:

    enum typeOfCommand {
        MINPWLEN = 1,
        MAXPWAGE = 2,
        MINPWAGE = 3,
        UNIQUEPW = 4,
        ACCOUNTLOCKOUTDURATION = 5,
        ACCOUNTLOCKOUTTHRESHOLD = 6,
        RESETLOCKOUTTIMER=7

    };

    passwordSetting();
    void setCommand(passwordSetting::typeOfCommand command);
    void setNumeric(int numeric);
    int getNumeric();
    void setWidget(QSpinBox *spinbox);
    std::string getCommandString();
    int getWidgetValue();

private:

    typeOfCommand _command;
    int _numeric;
    bool _boolean;
    QSpinBox * _spinbox;
};

#endif // passwordSetting_H
