#include "mainwindow.h"
#include <passwordSetting.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "ui_mainwindow.h"
#include "usersetting.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    maxpwage.setCommand(passwordSetting::MAXPWAGE);
    minpwage.setCommand(passwordSetting::MINPWAGE);
    minpwlen.setCommand(passwordSetting::MINPWLEN);
    uniquepw.setCommand(passwordSetting::UNIQUEPW);
    accountLockoutThresh.setCommand(passwordSetting::ACCOUNTLOCKOUTTHRESHOLD);
    accountLockoutDur.setCommand(passwordSetting::ACCOUNTLOCKOUTDURATION);
    resetLockoutTimer.setCommand(passwordSetting::RESETLOCKOUTTIMER);

   minpwage.setWidget(this->ui->minimumPasswordAge);
   maxpwage.setWidget(this->ui->maximumPasswordAge);
   minpwlen.setWidget(this->ui->minimumPasswordLength);
   uniquepw.setWidget(this->ui->numberOfStoredPasswords);
   accountLockoutThresh.setWidget(this->ui->lockoutThreshold);
   accountLockoutDur.setWidget(this->ui->lockoutDuration);
   resetLockoutTimer.setWidget(this->ui->lockoutResetTimer);

   system.setCommand(auditSetting::SYSTEM);
   logonoff.setCommand(auditSetting::LOGONOFF);
   objectacc.setCommand(auditSetting::OBJECTACC);
   priveledgeuse.setCommand(auditSetting::PRIVELEDGEUSE);
   tracking.setCommand(auditSetting::TRACKING);
   policychange.setCommand(auditSetting::POLCIYCHANGE);
   accountman.setCommand(auditSetting::ACCOUNTMAN);
   dsaccess.setCommand(auditSetting::DSACCESS);
   accountlogon.setCommand(auditSetting::ACCOUTLOGON);


   system.setWidget(this->ui->systemEventsS,this->ui->systeEventsF);
   logonoff.setWidget(this->ui->logonEventsS,this->ui->logonEventsF);
   objectacc.setWidget(this->ui->objectAccessS,this->ui->objectAccessF);
   priveledgeuse.setWidget(this->ui->privilegeUseS,this->ui->privilegeUseF);
   tracking.setWidget(this->ui->processTrackingS,this->ui->processTrackingF);
   policychange.setWidget(this->ui->policyChangeS,this->ui->policyChangeF);
   accountman.setWidget(this->ui->accountManageS,this->ui->accountManageF);
   dsaccess.setWidget(this->ui->dsaS,this->ui->dsaF);
   accountlogon.setWidget(this->ui->accountLogonS,this->ui->accountLogonS);

   disableUsers.setWidget(this->ui->usersList, this->ui->textEditAdmins,this->ui->disableGuest,this->ui->disableAdmin);
   disableUsers.setCommand(userSetting::DISABLEUSER);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_unlockButtonPasswords_clicked()
{

        disableUsers.determineBadAdmins();
        //disableUsers.determineBadUsers();
        //enumerateArrays();
        //cyberPatriot.setPasswordPolocies(passwordExecutes,sizeof(passwordExecutes));
        //cyberPatriot.setAuditPolocies(auditExecutes, sizeof(auditExecutes));
        //std::cout << minpwage.getWidgetValue();
}
void MainWindow::enumerateArrays(){

    passwordExecutes[0] = &maxpwage;
    passwordExecutes[1] = &minpwlen;
    passwordExecutes[2] = &uniquepw;
    passwordExecutes[3] = &minpwage;
    passwordExecutes[4] = &resetLockoutTimer;
    passwordExecutes[5] = &accountLockoutDur;
    passwordExecutes[6] = &accountLockoutThresh;

    auditExecutes[0] = &system;
    auditExecutes[1] = &logonoff;
    auditExecutes[2] = &objectacc;
    auditExecutes[3] = &priveledgeuse;
    auditExecutes[4] = &tracking;
    auditExecutes[5] = &policychange;
    auditExecutes[6] = &accountman;
    auditExecutes[7] = &dsaccess;
    auditExecutes[8] = &accountlogon;
}

