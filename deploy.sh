#!/bin/bash
logdir=~/.cyberpatriot_logs


echo "deploying dotfiles..." 
cd ~/  
git clone https://charlesdaniels@bitbucket.org/charlesdaniels/dotfiles.git 
cd ~/dotfiles
./deploy.sh 
echo "finished deploying dotfiles" 
cd ~/ 
echo "installing desired packages..." 

mkdir $logdir 

echo "performing system updates..." 
sudo apt-get update -y > $logdir/update.log
sudo apt-get upgrade -y > $logdir/upgrade.log
sudo apt-get dist-upgrade -y > $logdir/distupgrade.log
sudo apt-get autoremove -y > $logdir/autoremove.log

cd ~/cyberpatriot 
exec<"to_install" 
while read line 
do
	sudo apt-get install $line -y > $logdir/$line.log 
done
echo "done installing packages..." 

echo "searching for hacking tools..." 
exec<"hacking tools"
while read line
do 
	dpkg -l $line > $logdir/found_hacking_tools.txt
done
echo "done searching for hacking tools... please review ~/.cyberpatriot_logs/found_hacking_tools.txt" 

echo "beginning lynis scan..." 
sudo lynis --checkalli -Q --logfile $logdir/ynis.log &



