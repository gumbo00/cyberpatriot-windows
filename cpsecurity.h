#ifndef CPSECURITY_H
#define CPSECURITY_H
#include <string>
#include <iostream>
#include <sstream>
#include <QSpinBox>
#include "passwordSetting.h"
#include "auditsetting.h"
class CPSecurity
{
public:
    CPSecurity();
   bool setPasswordPolocies(passwordSetting *passwordExecutes[], int sizeOfArray);
   bool setAuditPolocies(auditSetting *auditExecutes[], int sizeOfArray);

private:

};

#endif // CPSECURITY_H
